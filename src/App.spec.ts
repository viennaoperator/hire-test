import * as supertest from 'supertest'
import app from './App'

describe('App', () => {
  it('app is working!', () =>
    supertest(app)
      .get('/')
        .expect('Content-Type', /html/)
        .expect(200)
  ),
      it('posting data to api works!', () => {
          var data = {
              "value" : "Hello World!",
              "numbers" : [1,2,3,4,5,50]
          }
          return supertest(app)
              .post('/')
              .send(data)
              .expect('Content-Type', /json/)
              .expect(200)
      }),
      it('getting index (test) of posted data!', () => {
          var data = {
              "value" : "Hello World!",
              "numbers" : [1,2,3,4,5,50,55,81]
          }
          return supertest(app)
              .post('/')
              .send(data)
              .expect('Content-Type', /json/)
              .expect('test', '1')
      }),
      it('getting filtered array', () => {
          var expectedFilteredArray = {
              "value" : "Hello World!",
              "numbers" : [1,2,3,4,5,50,81]
          }
          return supertest(app)
              .get('/test=1')
              .expect('Content-Type', /json/)
              .expect(expectedFilteredArray)
      })
})
