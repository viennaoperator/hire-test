import * as express from 'express';
import { Shelf } from "./model/Shelf";

class App {
    public app;
    private shelfs: Shelf[] = [];

    private static STANDARD_RESPONSE = 'Hello to this Demo Application \n' +
                                        'POST an JSON with Text and Number to Save \n' +
                                        'or GET for a specific number';

  constructor () {
      var bodyParser = require('body-parser');
      this.app = express();
      this.app.use(bodyParser.json()); // parse application/json
      this.app.locals.shelfs = this.shelfs;
      this.mountRoutes();
  }

  private mountRoutes (): void {
      this.app.get('/', function (request, response){
          response.send(App.STANDARD_RESPONSE);
      });

      this.app.post('/', function (request, response) {
          //could use better logger like https://www.npmjs.com/package/typescript-logging
          console.log('receiving data...');
          console.log('body is ',request.body);
          request.app.locals.shelfs.push(new Shelf(request.body.value, request.body.numbers));
          response.set('test',request.app.locals.shelfs.length - 1); //set index of array as response header
          response.json(request.body);
      });

      this.app.get('/test=:index', function (request, response) {
          let index = request.params.index;
          console.log('getting index : ' + index);
          console.log(request.app.locals.shelfs[index]);
          //filter the numbers according to the requirements
          request.app.locals.shelfs[index].numbers = request.app.locals.shelfs[index].numbers.filter(function(x){
              return (0 <= x && x <= 50) || x === 81;
          });
          response.send(request.app.locals.shelfs[index]);
      });
  }
}

export default new App().app

/*
general improvements: we could use a style checker that rewrites the code to a common style
for example delete all semicolons (or add) at the end of the line
 */