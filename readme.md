# Description

## Install Dependencies
```bash
npm install
```

## Build & Start
```bash
npm run build
npm run start
```

### Development

node will watch for changes and automatically refresh the application

```bash
npm run dev
```

### Running tests
```bash
npm test
```